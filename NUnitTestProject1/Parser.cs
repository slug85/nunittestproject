﻿using Newtonsoft.Json.Linq;
using NUnitTestProject1.http;
using RestSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Xml.Linq;


namespace NUnitTestProject1.test_data
{
    class Parser
    {
        private static IRequestFactory factory = new TestFactory();

        public static KeyValuePair<IRestRequest, IRestResponse>[] loadRequests()
        {

            string requests = NUnitTestProject1.Properties.Resources.requests;
            XDocument doc = XDocument.Parse(requests);

            List<KeyValuePair<IRestRequest, IRestResponse>> testList = new List<KeyValuePair<IRestRequest, IRestResponse>>();

            foreach (XElement el in doc.Root.Elements())
            {
                IRestRequest request = parseRequest(el.Element("request"));
                IRestResponse response = parseResponse(el.Element("response"));
                KeyValuePair<IRestRequest, IRestResponse> test = new KeyValuePair<IRestRequest, IRestResponse>(request, response);
                testList.Add(test);
            }

            //массив объектов для параметризованного теста nunit
            KeyValuePair<IRestRequest, IRestResponse>[] testObjects = new KeyValuePair<IRestRequest, IRestResponse>[testList.Count];
            for (int n = 0; n < testList.Count; n++)
            {
                testObjects[n] = testList[n];
            }

            return testObjects;

        }

        private static IRestRequest parseRequest(XElement el)
        {
            string method = el.Element("method").Value.ToLower();
            string text = el.Element("text").Value;
            string serviceType = el.Element("serviceType").Value.ToLower();

            Method m;
            switch (method)
            {
                case "post":
                    m = Method.POST;
                    break;
                case "get":
                    m = Method.GET;
                    break;
                default:
                    m = Method.GET;
                    break;

            }

            ServiceType type;
            switch (serviceType)
            {
                case "xml":
                    type = ServiceType.XML;
                    break;
                case "json":
                    type = ServiceType.JSON;
                    break;
                default:
                    type = ServiceType.XML;
                    break;

            }
            IRestRequest request = factory.createRequest(m, type, text);
            return request;

        }
        private static IRestResponse parseResponse(XElement el)
        {
            int status = int.Parse(el.Element("status").Value);
            SpellerResponse response = new SpellerResponse();
            List<SpellerError> errorList = new List<SpellerError>();
            XElement errors = el.Element("errors");
            if (errors != null)
            {
                response.Errors = parseError(errors);
            }
            response.StatusExpected = (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), status.ToString());
            return response as IRestResponse;
        }

        private static List<SpellerError> parseError(XElement errors)
        {
            if (errors == null) return null;
            List<SpellerError> errorList = new List<SpellerError>();
            foreach (XElement error in errors.Elements())
            {
                int code = int.Parse(error.Attribute("code").Value);
                string word = error.Element("word") == null ? null : error.Element("word").Value; ;
                string s = error.Element("s") == null ? null : error.Element("s").Value;
                SpellerError spellerError = new SpellerError(code, word, s);
                errorList.Add(spellerError);
            }
            if (errorList.Count > 0) return errorList;
            else return null;
        }

        private static List<SpellerError> parseError(JArray errors)
        {
            List<SpellerError> errorList = new List<SpellerError>();

            foreach (JObject error in errors)
            {
                string s = error["s"].HasValues ? (string)error["s"][0] : null;
                string word = (string)error["word"];
                int code = (int)error["code"];
                SpellerError err = new SpellerError(code, word, s);
                errorList.Add(err);
            }
            if (errorList.Count > 0) return errorList;
            else return null;
        }

        public static List<SpellerError> parseError(IRestResponse response)
        {
            if (response.Content.Length != 0)
            {
                if (response.ContentType.Contains("application/json;"))
                {
                    JArray root = JArray.Parse(response.Content);
                    return parseError(root);
                }
                else if (response.ContentType.Contains("xml"))
                {
                    XElement errors = XDocument.Parse(response.Content).Root;
                    return parseError(errors);

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }

        }
    }
}
