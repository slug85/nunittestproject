﻿using System;
using NUnit.Framework;
using NUnitTestProject1.http;
using NUnitTestProject1.test_data;
using RestSharp;
using System.Collections.Generic;
using System.Xml.Linq;

namespace NUnitTestProject1
{
    [TestFixture]
    public class NUnitTest
    {


        public static KeyValuePair<IRestRequest, IRestResponse>[] LoadData()
        {
            return Parser.loadRequests();
        }


        [Test]
        [TestCaseSource("LoadData")]
        public void Test(KeyValuePair<IRestRequest, IRestResponse> test)
        {

            SpellerResponse expectedResponse = test.Value as SpellerResponse;
            SpellerRequest spellerRequest = test.Key as SpellerRequest;


            IRestResponse response = Client.getInstance().Execute(spellerRequest);

            //проверить статус ответа на совпадаение с ожидаемым
            Assert.AreEqual(expectedResponse.StatusExpected, response.StatusCode);

            //получить актуальный список ошибок из тела ответа и сравнить с ожидаемым из файла requests.xml
            List<SpellerError> actualErrors = Parser.parseError(response);
            CollectionAssert.AreEqual(expectedResponse.Errors, actualErrors);

        }
    }
}