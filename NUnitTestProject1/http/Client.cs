﻿using System;
using RestSharp;
using System.Configuration;
using System.Net;

namespace NUnitTestProject1.http
{
    class Client : RestClient
    {
        private static readonly Lazy<Client> instance = new Lazy<Client>(() => new Client());

        private Client()
        {
            try
            {
                base.BaseUrl = new Uri(ConfigurationManager.AppSettings.Get("baseUrl"));

            }
            catch (UriFormatException e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message + System.Environment.NewLine + e.StackTrace);
            }
            bool proxy = Boolean.Parse(ConfigurationManager.AppSettings.Get("proxy"));
            if (proxy)
            {
                string proxyHost = ConfigurationManager.AppSettings.Get("proxy.host");
                string proxyUser = ConfigurationManager.AppSettings.Get("proxy.user");
                string proxyPassword = ConfigurationManager.AppSettings.Get("proxy.password");
                ICredentials credentials = new NetworkCredential(proxyUser, proxyPassword);
                this.Proxy = new WebProxy(proxyHost, true, null, credentials);
            }
        }

        public static Client getInstance()
        {
            return instance.Value;
        }

        public override string ToString()
        {
            return "baseUrl: " + this.BaseUrl;
        }

    }


}


