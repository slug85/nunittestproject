﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTestProject1.http
{
    class SpellerError
    {
        public SpellerError(int code, string word, string s)
        {
            Code = code;
            Word = word;
            S = s;
        }

        public int Code { get; set; }
        public string Word { get; set; }
        public string S { get; set; }

        public override bool Equals(System.Object obj)
        {
            if (obj is SpellerError)
            {
                return
                    this.Code == (obj as SpellerError).Code
                    && this.Word == (obj as SpellerError).Word
                    && this.S == (obj as SpellerError).S;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return this.Code.GetHashCode() + this.Word.GetHashCode() + this.S.GetHashCode();
        }
    }
}
