﻿using RestSharp;

namespace NUnitTestProject1.http
{
    interface IRequestFactory
    {
        SpellerRequest createRequest(Method method, ServiceType type, string text);
        SpellerResponse createResponse(string text, int status);
    }
}
