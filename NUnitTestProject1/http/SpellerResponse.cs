﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTestProject1.http
{
    class SpellerResponse:RestResponse
    {
        public HttpStatusCode StatusExpected { get; set; }
        public List<SpellerError> Errors { get; set; }
    }
}
