﻿using RestSharp;
using System;
using System.Configuration;
using System.Net;

namespace NUnitTestProject1.http
{
    class TestFactory : IRequestFactory
    {
        private string json;
        private string service;
        private string xml;

        public TestFactory()
        {
            xml = ConfigurationManager.AppSettings.Get("xml");
            json = ConfigurationManager.AppSettings.Get("json");
            service = ConfigurationManager.AppSettings.Get("service");
        }

        public SpellerResponse createResponse(string text, int status)
        {
            SpellerResponse response = new SpellerResponse();
            response.Content = text;

            //expected status code из xml
            try
            {
                response.StatusExpected = (HttpStatusCode)System.Enum.Parse(typeof(HttpStatusCode), status.ToString());
            }
            catch (ArgumentException e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message + System.Environment.NewLine + e.StackTrace);
                System.Diagnostics.Trace.WriteLine("default expected status: 200");
                response.StatusExpected = HttpStatusCode.OK;
            }
            return response;


        }

        public SpellerRequest createRequest(Method method, ServiceType type, string text)
        {
            SpellerRequest request = new SpellerRequest();
            request.ServiceType = type;
            request.Method = method;

            switch (request.Method)
            {
                case Method.POST:
                    request.Resource = "{servicetype}/{servicename}";
                    request.AddParameter("text", text);
                    break;
                case Method.GET:
                    request.Resource = "{servicetype}/{servicename}?text={text}";
                    request.AddUrlSegment("text", text);
                    break;
            }

            //XML-интерфейс: http://speller.yandex.net/services/spellservice/checkText?text=синхрафазатрон+в+дубне
            //JSON-интерфейс: http://speller.yandex.net/services/spellservice.json/checkText?text=синхрафазатрон+в+дубне
            switch (type)
            {
                case ServiceType.JSON:
                    request.AddUrlSegment("servicetype", json);
                    break;
                case ServiceType.XML:
                    request.AddUrlSegment("servicetype", xml);
                    break;
            }

            request.AddUrlSegment("servicename", service);
            return request;
        }
    }
}
