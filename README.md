### Набор тестов для веб-сервиса Яндекс.Спеллер.

XML-интерфейс:
http://speller.yandex.net/services/spellservice/checkText?text=синхрафазатрон+в+дубне

JSON-интерфейс:
http://speller.yandex.net/services/spellservice.json/checkText?text=синхрафазатрон+в+дубне

### Настройки проекта
В файле настроек App.config можно задать 

```sh
базовый адрес сервиса (для возможности запуска тестов на тестовых стендах) 
<add key="baseUrl" value="http://speller.yandex.net/services/" />
```

```sh
настройки прокcи
<add key="proxy" value="false" />
<add key="proxy.user" value="sergey.lugovskoi" />
<add key="proxy.password" value="*****" />
<add key="proxy.host" value="tmg.soglasie.ru:8080" />
```

### Тестовые данные
Наборы данных для отправки тестовых запросов и проверок корректности ответов хранятся в файле assets/requests.xml
например

```sh
  <test>
    <request>
      <method>GET</method>
      <text>синхрафазатрон+в+дубне</text>
      <serviceType>XML</serviceType>
    </request>
    <response>
      <errors>
        <error code="1">
          <word>синхрафазатрон</word>
          <s>синхрофазотрон</s>
        </error>
        <error code="3">
          <word>дубне</word>
          <s>Дубне</s>
        </error>
      </errors>
      <status>200</status>
    </response>
  </test>
```

#### Запуск
Запуск тестов можно осуществить с помошью Nunit GUI или из Visual Studio через NUnitTestAdapte 2.0.0
Список необходимых для запуска пакетов:

```sh
<packages>
  <package id="Newtonsoft.Json" version="8.0.3" targetFramework="net452" />
  <package id="NUnit" version="2.6.1" targetFramework="net452" />
  <package id="NUnitTestAdapter" version="2.0.0" targetFramework="net452" />
  <package id="RestSharp" version="105.2.3" targetFramework="net452" />
</packages>
```

